﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Windows.Media.Imaging;
//------------------------------
using TestJson.Json.Converters;
//------------------------------
using Newtonsoft.Json;

namespace TestJson
{
    public class LabelWithText
    {
        public string Label { get; set; }
        public string Text  { get; set; }
    }

    public class KeyCrimeCardData
    {
        // N.B.: gli oggetti complessi vengono creati dal ctor per evitare problemi di "null object"
        public KeyCrimeCardData()
        {
            this.Series     = new CardSeries();
            this.EventTab   = new EventCard();
            this.AuthorTab  = new List<AuthorCard>();
            this.OutfitTab  = new ClothingCard();
            this.VehicleTab = new List<VehicleCard>();
            this.WeaponTab  = new List<WeaponCard>();
            this.VictimTab  = new List<VictimCard>();
        }

        public string            CardType   { get; set; }       // Rapina, Violenza, Autore, ...
        public string            Title      { get; set; }       // Titolo scheda (es.: Farmacia ...)
        public CardSeries        Series     { get; set; }       // Serie
        public EventCard         EventTab   { get; set; }       // Dati evento
        public List<AuthorCard>  AuthorTab  { get; set; }       // Autori
        public ClothingCard      OutfitTab  { get; set; }       // Abbigliamento
        public List<VehicleCard> VehicleTab { get; set; }       // Mezzi
        public List<WeaponCard>  WeaponTab  { get; set; }       // Armi
        public List<VictimCard>  VictimTab  { get; set; }       // Vittime
    }

    public class CardSeries
    {
        public string Name { get; set; }

        [JsonConverter(typeof(JsonImageConverter))]
        public BitmapImage Icon { get; set; }
    }

    public class EventCard
    {
        public EventCard()
        {
            this.GeneralInfo = new Geninfo();
            this.Target      = new TargetInfo();
            this.Goods       = new StolenGoods();
        }

        public Geninfo      GeneralInfo { get; set; }   // Informazioni generali
        public TargetInfo   Target      { get; set; }   // Parte lesa
        public StolenGoods  Goods       { get; set; }   // Bene sottratto
        public string       Description { get; set; }   // Descrizione libera
    }

    public class Geninfo
    {
        public Geninfo()
        {
            this.Location         = new LabelWithText();
            this.Authority        = new LabelWithText();
            this.TimeDuration     = new LabelWithText();
            this.WeatherCondition = new LabelWithText();
        }

        public string        SectionTitle     { get; set; }
        public LabelWithText Location         { get; set; }
        public LabelWithText Authority        { get; set; }
        public LabelWithText TimeDuration     { get; set; }     //	Periodo : <data>, alle ore hh:mm durata :
        public LabelWithText WeatherCondition { get; set; }     // : <condizioni meteo>
    }

    /// <summary>
    /// Parte lesa
    /// </summary>
    public class TargetInfo
    {
        public TargetInfo()
        {
            this.TargetType = new LabelWithText();
            this.Chain      = new LabelWithText();
            this.Name       = new LabelWithText();
        }

        public string        SectionTitle { get; set; }
        public LabelWithText TargetType   { get; set; }     // Tipo (es.: Banca)
        public LabelWithText Chain        { get; set; }     // Catena commerciale
        public LabelWithText Name         { get; set; }     // Nome attività
    }

    public class StolenGoods
    {
        public StolenGoods()
        {
            this.GoodsList = new List<StolenGoodsInfo>();
        }

        public string                SectionTitle { get; set; }     // Bene sottratto
        public List<StolenGoodsInfo> GoodsList    { get; set; }     // Elenco beni sottratti
    }

    public class StolenGoodsInfo
    {
        public StolenGoodsInfo()
        {
            this.Details = new List<LabelWithText>();
        }

        public string               SectionTitle { get; set; }      // Es.: Orologio, Denaro, Cellulare
        public List<LabelWithText>  Details      { get; set; }
    }

    public class AuthorCard
    {
        public AuthorCard()
        {
            this.Author              = new AuthorData();
            this.Language            = new Idiom();
            this.Ethnic              = new Ethnicity();
            this.Body                = new PhysicalAttributes();
            this.PhysicalFeatures    = new PhysicalFeature();
            this.DistinguishingMarks = new DistinguishingMark();
            this.BehavioralProfile   = new LabelWithText();
        }

        public string                SectionTitle           { get; set; }
        public AuthorData            Author                 { get; set; }
        public Idiom                 Language               { get; set; }
        public Ethnicity             Ethnic                 { get; set; }
        public PhysicalAttributes    Body                   { get; set; }   // corporatura, altezza, età...
        public PhysicalFeature       PhysicalFeatures       { get; set; }   // Fisionomia: Capelli, bocca., orecchie....
        public DistinguishingMark    DistinguishingMarks    { get; set; }	// segni particolari 
        public LabelWithText         BehavioralProfile      { get; set; }   // Immagine creata da impronta comportamentale
    }

    public class PhysicalFeature
    {
        public PhysicalFeature()
        {
            this.Details = new List<PhysicalFeatureDetail>();
        }

        public string                      SectionTitle { get; set; }   // Fisionomia
        public List<PhysicalFeatureDetail> Details      { get; set; }
    }

    public class PhysicalFeatureDetail
    {
        public PhysicalFeatureDetail()
        {
            this.Type       = new LabelWithText();
            this.Category   = new LabelWithText();
            this.Color      = new LabelWithText();
            this.Particular = new LabelWithText();
        }

        public string        SectionTitle { get; set; }     // Es.: Capelli, Bocca, Orecchie, Occhi
        public LabelWithText Type         { get; set; }
        public LabelWithText Category     { get; set; }
        public LabelWithText Color        { get; set; }
        public LabelWithText Particular   { get; set; }

    }

    public class DistinguishingMark
    {
        public DistinguishingMark()
        {
            this.Marks = new List<MarkFeature>();
        }

        public string            SectionTitle { get; set; }     // Segni particolari
        public List<MarkFeature> Marks        { get; set; }     // Elenco
    }

    public class MarkFeature
    {
        public MarkFeature()
        {
            this.Type        = new LabelWithText();
            this.Properties  = new LabelWithText();
            this.Detail      = new LabelWithText();
            this.Description = new LabelWithText();
        }

        public string        SectionTitle { get; set; }     // Es.: Denti, Paresi, Tic nervoso
        public LabelWithText Type         { get; set; }
        public LabelWithText Properties   { get; set; }
        public LabelWithText Detail       { get; set; }
        public LabelWithText Description  { get; set; }
    }

    public class AuthorData
    {
        public AuthorData()
        {
            this.AuthorName  = new LabelWithText();
            this.DateOfBirth = new LabelWithText();
        }

        public string        SectionTitle { get; set; }
        public LabelWithText AuthorName   { get; set; }     // 'first name, last name, gender',
        public LabelWithText DateOfBirth  { get; set; }
    }

    public class Idiom
    {
        public Idiom()
        {
            this.Name    = new LabelWithText();
            this.GeoArea = new LabelWithText();
        }

        public string        SectionTitle { get; set; }
        public LabelWithText Name         { get; set; }
        public LabelWithText GeoArea      { get; set; }
    }

    public class Ethnicity
    {
        public Ethnicity()
        {
            this.Type = new LabelWithText();
            this.Skin = new LabelWithText();
        }

        public string        SectionTitle { get; set; }
        public LabelWithText Type         { get; set; }     // Es.: Italiana/Europea
        public LabelWithText Skin         { get; set; }     // Es.: Chaira/Pallida
    }

    public class ClothingCard
    {
        public ClothingCard()
        {
            this.Clothes = new ClothesSection();
            this.Extras  = new AccessorySection();
        }

        public string           SectionTitle { get; set; }      // Abbigliamento
        public ClothesSection   Clothes      { get; set; }
        public AccessorySection Extras       { get; set; }
    }

    public class ClothesSection
    {
        public ClothesSection()
        {
            this.Details = new List<Characteristic>();
        }

        public string               SectionTitle { get; set; }      // Giacca, Jeans, ...
        public List<Characteristic> Details      { get; set; }
    }

    public class AccessorySection
    {
        public AccessorySection()
        {
            this.Accessories = new List<Characteristic>();
        }

        public string                SectionTitle { get; set; }      // Accessori
        public List<Characteristic>  Accessories  { get; set; }
    }

    public class VehicleCard
    {
        public VehicleCard()
        {
            this.VehicleType = new LabelWithText();
            this.Info        = new LabelWithText();
            this.Plate       = new LabelWithText();
            this.PlateType   = new LabelWithText();
            this.Features    = new List<Particular>();
        }

        public string           SectionTitle { get; set; }
        public LabelWithText    VehicleType  { get; set; }
        public LabelWithText    Info         { get; set; }      // categoria, modello marca
        public LabelWithText    Plate        { get; set; }
        public LabelWithText    PlateType    { get; set; }
        public List<Particular> Features     { get; set; }
    }

    public class WeaponCard
    {
        public WeaponCard()
        {
            this.WeaponType  = new Characteristic();
            this.Particulars = new List<Characteristic>();
        }

        public string               SectionTitle { get; set; }
        public Characteristic       WeaponType   { get; set; }
        public List<Characteristic> Particulars  { get; set; }
    }

    public class VictimCard
    {
        public VictimCard()
        {
            this.VictimName   = new LabelWithText();
            this.Alias        = new LabelWithText();
            this.Birthplace   = new LabelWithText();
            this.Nationality  = new LabelWithText();
            this.Telephone    = new LabelWithText();
            this.Mobile       = new LabelWithText();
            this.Language     = new Idiom();
            this.Features     = new Ethnicity();
            this.PhysicalAttr = new PhysicalAttributes();
            this.Physiognomy  = new Characteristic();
            this.Clothes      = new List<Characteristic>();
            this.Accessories  = new List<Characteristic>();
        }

        public string               SectionTitle    { get; set; }
        public LabelWithText        VictimName      { get; set; } //  'first name, last name, gender',
        public LabelWithText        Alias           { get; set; }
        public string               DateOfBirth     { get; set; }
        public LabelWithText        Birthplace      { get; set; }
        public LabelWithText        Nationality     { get; set; }
        public LabelWithText        Telephone       { get; set; }
        public LabelWithText        Mobile          { get; set; }
        public Idiom                Language        { get; set; }
        public Ethnicity            Features        { get; set; }
        public PhysicalAttributes   PhysicalAttr    { get; set; }
        public Characteristic       Physiognomy     { get; set; }
        public List<Characteristic> Clothes         { get; set; }
        public List<Characteristic> Accessories     { get; set; }
    }

    public class PhysicalAttributes
    {
        public PhysicalAttributes()
        {
            this.Height      = new LabelWithText();
            this.Age         = new LabelWithText();
            this.Build       = new LabelWithText();
            this.Description = new LabelWithText();
        }

        public string        SectionTitle { get; set; }
        public LabelWithText Build        { get; set; }
        public LabelWithText Height       { get; set; }
        public LabelWithText Age          { get; set; }
        public LabelWithText Description  { get; set; }
    }

    public class Characteristic     // Struttura dati per Capelli, Bocca, Orecchie, Occhi...
    {
        public Characteristic()
        {
            this.Name         = new LabelWithText();
            this.PartType     = new LabelWithText();
            this.Description1 = new LabelWithText();
            this.Description2 = new LabelWithText();
            this.Description3 = new LabelWithText();
        }

        public LabelWithText Name         { get; set; }     // Es.: Pelliccia, Station wagon, Coltello
        public LabelWithText PartType     { get; set; }     // Es.: Pelliccia, Jeans, Ford, "da cucina", ...
        public LabelWithText Description1 { get; set; }     // scuro, avanti destra, Ford, a scatto, ...
        public LabelWithText Description2 { get; set; }     // scuro, chiaro, -
        public LabelWithText Description3 { get; set; }     // spalle calate, con risvolto, ...
    }

    public class Particular
    {
        public Particular()
        {
            this.Name         = new LabelWithText();
            this.PartType     = new LabelWithText();
            this.Description1 = new LabelWithText();
            this.Description2 = new LabelWithText();
            this.Description3 = new LabelWithText();
        }

        public LabelWithText Name         { get; set; }     // Es.: Pelliccia, Station wagon, Coltello
        public LabelWithText PartType     { get; set; }     // Es.: Pelliccia, Jeans, Ford, "da cucina", ...
        public LabelWithText Description1 { get; set; }     // scuro, avanti destra, Ford, a scatto, ...
        public LabelWithText Description2 { get; set; }     // scuro, chiaro, -
        public LabelWithText Description3 { get; set; }     // spalle calate, con risvolto, ...
    }

    /// <summary>
    /// Classe dati per elenco di righe di stringhe (es.: "Pelliccia;colore principale;scuro#CD031D;spalle calate"
    /// ogni riga ha SEMPRE 5 valori
    /// </summary>
    public class DataRow
    {
        public DataRow()
        {
            this.ItemArray = new string[5];
        }

        string[] ItemArray { get; set; }
    }
}