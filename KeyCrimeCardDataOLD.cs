﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace TestJsonOLD
{
    class KeyCrimeCardDataOLD
    {
        public KeyCrimeCardDataOLD()
        {
            this.Series = new CardSeries();
            this.EventData = new EventCard();
            this.Authors = new List<Author>();
            this.Dress = new Clothing();
            this.Vehicles = new List<Vehicle>();
            this.Weapons = new List<Weapon>();
            this.Victims = new List<Victim>();
        }

        public string           CardType  { get; set; }     // Rapina, Violenza, Autore, ...
        public string           Title     { get; set; }     // Titolo scheda (es.: Farmacia ...)
        public CardSeries       Series    { get; set; }     // Serie
        public EventCard        EventData { get; set; }     // Dati evento
        public List<Author>     Authors   { get; set; }     // Autori
        public Clothing         Dress     { get; set; }     // Abbigliamento
        public List<Vehicle>    Vehicles  { get; set; }     // Mezzi
        public List<Weapon>     Weapons   { get; set; }     // Armi
        public List<Victim>     Victims   { get; set; }     // Vittime
    }

    public class CardSeries
    {
        public string Name { get; set; }
        public Image  Icon { get; set; }
    }

    public class EventCard
    {
        public EventCard()
        {
            this.Goods = new List<CardStolenGood>();
            this.FreeDescription = new List<string>();
        }

        public Geninfo GeneralInfo { get; set; }                // Informazioni generali
        public Aggrieved AggrievedInfo { get; set; }            // Parte lesa
        public List<CardStolenGood> Goods { get; set; }         // Bene sottratto
        public List<string> FreeDescription { get; set; }       // Descrizione libera
    }

    public class Geninfo
    {
        public string Location { get; set; }
        public string Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Duration { get; set; }
        public string WeatherCondition { get; set; }
        public string Luminosity { get; set; }
        public string Authority { get; set; }
    }

    /// <summary>
    /// Parte lesa
    /// </summary>
    public class Aggrieved
    {
        public string AggrievedType { get; set; }   // Tipo (es.: Banca)
        public string Chain { get; set; }           // Catena commerciale
        public string Name { get; set; }            // Nome attività
    }

    public class CardStolenGood
    {
        public CardStolenGood()
        {
            this.Details = new List<CardStolenGoodDetails>();
        }

        public string Name { get; set; }
        public List<CardStolenGoodDetails> Details { get; set; }
    }

    public class CardStolenGoodDetails
    {
        public string Type { get; set; }
        public string Value { get; set; }
    }

    public class Author
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Sex { get; set; }
        public Idiom Language { get; set; }
        public Ethnicity Origin { get; set; }
        public List<Characteristic> PhysicalAttributes { get; set; }
        public List<Characteristic> PhysicalParticular { get; set; }
        public Image BehavioralProfile { get; set; }
    }

    public class Idiom
    {
        public string Name { get; set; }
        public string GeoArea { get; set; }
    }

    public class Ethnicity
    {
        public string type { get; set; }    // Es.: Italiana/Europea
        public string skin { get; set; }    // Es.: Chaira/Pallida
    }

    public class Clothing
    {
        public List<Characteristic> Clothes { get; set; }
        public List<Characteristic> Accessories { get; set; }
    }

    public class Vehicle
    {
        public Vehicle()
        {
            this.Features = new List<Particular>();
        }

        public string VehicleType { get; set; }
        public string Plate { get; set; }
        public string PlateType { get; set; }
        public List<Particular> Features { get; set; }
    }

    public class Weapon
    {
        public Weapon()
        {
            this.WeaponType = new Characteristic();
            this.Perticulars = new List<Characteristic>();
        }

        public Characteristic WeaponType { get; set; }
        public List<Characteristic> Perticulars { get; set; }
    }

    public class Victim
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string alias { get; set; }
        public DateTime Birthday { get; set; }
        public string Birthplace { get; set; }
        public string Citizenship { get; set; }
        public string telephone { get; set; }
        public string mobile { get; set; }
        public Idiom Language { get; set; }
        public Ethnicity Features { get; set; }
        public PhysicalAttr PhysicalAttributes { get; set; }
        public Characteristic Physiognomy { get; set; }
        public List<Characteristic> Clothes { get; set; }
        public List<Characteristic> Accessories { get; set; }
    }

    public class PhysicalAttr
    {
        public int Height { get; set; }
        public int Age { get; set; }
        public string Build { get; set; }
        public string Description { get; set; }
    }

    public class Characteristic     // Struttura dati per Capelli, Bocca, Orecchie, Occhi...
    {
        public Characteristic()
        {
            this.Particulars = new List<Particular>();
        }

        public string Name { get; set; }
        public List<Particular> Particulars { get; set; }
    }

    public class Particular
    {
        public string Name { get; set; }
        public string Color { get; set; }
    }
}

