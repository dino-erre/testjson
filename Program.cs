﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
//------------------------------
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TestJson
{
    /// <summary>
    /// Test serializzazione Json...per doc googolare "newtonsoft json serialize"
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            string jsonFile = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\", @"Data\", "Test.json"));

            KeyCrimeCardData data = getData();

            JsonSerializer serializer = new JsonSerializer();
            serializer.Converters.Add(new JavaScriptDateTimeConverter());
            serializer.NullValueHandling = NullValueHandling.Include;
            serializer.Formatting = Formatting.Indented;

            using (StreamWriter sw = new StreamWriter(jsonFile))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, data);
            }
        }

        private static KeyCrimeCardData getData()
        {
            KeyCrimeCardData result = new KeyCrimeCardData();

            result.CardType    = "Robbery";
            result.Title       = "Farmacia ... quella lì";
            //---------- Series ----------
            result.Series.Name = "Xx serie";
            result.Series.Icon = getImage();
            //---------- EventTab GenInfo ----------
            result.EventTab.GeneralInfo.SectionTitle = "Informazioni generali";
            result.EventTab.GeneralInfo.Location.Label = "Luogo: ";
            result.EventTab.GeneralInfo.Location.Text  = "Milano, via della Mela, 2";
            result.EventTab.GeneralInfo.TimeDuration.Label = "Periodo";
            result.EventTab.GeneralInfo.TimeDuration.Text  = "25/3/2018, alle ore 12.50, per 45 min";
            result.EventTab.GeneralInfo.Authority.Label = "Organo intervenuto";
            result.EventTab.GeneralInfo.Authority.Text  = "Polizia";
            result.EventTab.GeneralInfo.WeatherCondition.Label = "Visibilità";
            result.EventTab.GeneralInfo.WeatherCondition.Text  = "Pioggia, Imbrunire";
            //---------- EventTab TargetInfo ----------
            result.EventTab.Target.SectionTitle = "Parte lesa";
            result.EventTab.Target.TargetType.Label = "Tipologia";
            result.EventTab.Target.TargetType.Text  = "Banca";
            result.EventTab.Target.Chain.Label = "Catena commerciale";
            result.EventTab.Target.Chain.Text  = "Monte dei Paschi di Siena";
            result.EventTab.Target.Name.Label  = "Nome attività";
            result.EventTab.Target.Name.Text   = "Istituto 2 Corso Lodi";
            //---------- EventTab StolenGoodsInfo ----------
            StolenGoods sg = new StolenGoods();
            sg.SectionTitle = "Bene sottratto";
            StolenGoodsInfo sgi_0 = new StolenGoodsInfo();
            sgi_0.SectionTitle = "Orologio";
            LabelWithText sgi_0_0 = new LabelWithText();
            sgi_0_0.Label = "Swatch";
            sgi_0_0.Text  = "300€";
            sgi_0.Details.Add(sgi_0_0);
            LabelWithText sgi_0_1 = new LabelWithText();
            sgi_0_1.Label = "Rolex";
            sgi_0_1.Text  = "400€";
            sgi_0.Details.Add(sgi_0_1);
            StolenGoodsInfo sgi_1 = new StolenGoodsInfo();
            sgi_1.SectionTitle = "Denaro";
            LabelWithText sgi_1_0 = new LabelWithText();
            sgi_1_0.Label = string.Empty;
            sgi_1_0.Text  = "400€";
            sgi_1.Details.Add(sgi_1_0);
            StolenGoodsInfo sgi_2 = new StolenGoodsInfo();
            sgi_1.SectionTitle = "Cellulare";
            LabelWithText sgi_2_0 = new LabelWithText();
            sgi_2_0.Label = "Samsung";
            sgi_2_0.Text  = "550€";
            sgi_2.Details.Add(sgi_2_0);
            //----------
            sg.GoodsList.Add(sgi_0);
            sg.GoodsList.Add(sgi_1);
            sg.GoodsList.Add(sgi_2);
            result.EventTab.Goods = sg;
            //---------- EventTab Description ----------
            result.EventTab.Description = "Descrizione libera...quella taluni chiama anche 'denuncia' :)";
            //---------- AuthorTab (elenco) ----------
            AuthorCard ac_0 = new AuthorCard();
            ac_0.SectionTitle = "Autore 01 (noto)";
            ac_0.Author.SectionTitle = "Informazioni generali";
            ac_0.Author.AuthorName.Label = "Nome";
            ac_0.Author.AuthorName.Text  = "Andrea Maldini, Uomo";
            ac_0.Author.DateOfBirth.Label = "Data di nascita";
            ac_0.Author.DateOfBirth.Text  = "25/08/1976";
            //----------
            ac_0.Language.SectionTitle = "Inflessione linguistica";
            ac_0.Language.Name.Label = "Lingua";
            ac_0.Language.Name.Text  = "Italiano";
            ac_0.Language.GeoArea.Label = "Area geografica";
            ac_0.Language.GeoArea.Text  = "Europa";
            //----------
            ac_0.Ethnic.SectionTitle = "Tratti somatici";
            ac_0.Ethnic.Type.Label = "Etnia";
            ac_0.Ethnic.Type.Text  = "Italiana/Europea";
            ac_0.Ethnic.Skin.Label = "Carnagione";
            ac_0.Ethnic.Skin.Text  = "Chiara/Pallida";
            //----------
            ac_0.Body.SectionTitle = "Attributi fisici";
            ac_0.Body.Build.Label = "Corporatura";
            ac_0.Body.Build.Text  = "media/robusta";
            ac_0.Body.Height.Label = "Altezza";
            ac_0.Body.Height.Text  = "1.80/1.85 m";
            ac_0.Body.Age.Label = "Età";
            ac_0.Body.Age.Text  = "25/26/30";
            ac_0.Body.Description.Label = "Descrizione";        
            ac_0.Body.Description.Text  = "Bel figaccione";
            //----------
            ac_0.PhysicalFeatures.SectionTitle = "Fisionomia";
            PhysicalFeatureDetail pf_0 = new PhysicalFeatureDetail();
            pf_0.SectionTitle = "Capelli";
            pf_0.Type.Label = "";
            pf_0.Type.Text  = "Corti";
            pf_0.Category.Label = "";
            pf_0.Category.Text  = "mossi";
            pf_0.Color.Label = "";
            pf_0.Color.Text  = "biondi scuri";
            pf_0.Particular.Label = "";
            pf_0.Particular.Text  = "con gel";
            PhysicalFeatureDetail pf_1 = new PhysicalFeatureDetail();
            pf_1.SectionTitle = "Bocca";
            pf_1.Type.Label = "";
            pf_1.Type.Text  = "Piccola";
            pf_1.Category.Label = "";
            pf_1.Category.Text  = "storta";
            pf_1.Color.Label = "";
            pf_1.Color.Text  = "";
            pf_1.Particular.Label = "";
            pf_1.Particular.Text  = "";

            ac_0.PhysicalFeatures.Details.Add(pf_0);
            ac_0.PhysicalFeatures.Details.Add(pf_1);
            //----------
            ac_0.DistinguishingMarks.SectionTitle = "Segni particolari";
            MarkFeature mf_0 = new MarkFeature();
            mf_0.SectionTitle = "Denti";
            mf_0.Type.Label = "";
            mf_0.Type.Text  = "Arcata sup.";
            mf_0.Properties.Label = "";
            mf_0.Properties.Text  = "centro";
            mf_0.Detail.Label = "";
            mf_0.Detail.Text  = "finti";
            mf_0.Description.Label = "";
            mf_0.Description.Text  = "";
            MarkFeature mf_1 = new MarkFeature();
            mf_1.SectionTitle = "Paresi";
            mf_1.Type.Label = "";
            mf_1.Type.Text  = "Gamba dx";
            mf_1.Properties.Label = "";
            mf_1.Properties.Text  = "";
            mf_1.Detail.Label = "";
            mf_1.Detail.Text  = "";
            mf_1.Description.Label = "";
            mf_1.Description.Text  = "";

            ac_0.DistinguishingMarks.Marks.Add(mf_0);
            ac_0.DistinguishingMarks.Marks.Add(mf_1);

            ac_0.BehavioralProfile.Label = "Profilo comportamentale";
            ac_0.BehavioralProfile.Text = string.Empty;
            result.AuthorTab.Add(ac_0);
            //---------- Clothing card ----------
            ClothingCard clothCard = new ClothingCard();
            ClothesSection cs_0 = new ClothesSection();
            Characteristic cs_0_0 = new Characteristic();
            clothCard.SectionTitle = "Abbigliamento";
            cs_0.SectionTitle = "Giacca";
            cs_0_0.Name.Label = "";
            cs_0_0.Name.Text  = "Pelliccia";
            cs_0_0.PartType.Label = "";
            cs_0_0.PartType.Text  = "colore principale";
            cs_0_0.Description1.Label = "scuro";
            cs_0_0.Description1.Text  = "#CD031D";
            cs_0_0.Description2.Label = "";
            cs_0_0.Description2.Text  = "";
            cs_0_0.Description3.Label = "";
            cs_0_0.Description3.Text  = "";
            Characteristic cs_0_1 = new Characteristic();
            cs_0_1.Name.Label = "";
            cs_0_1.Name.Text  = "";
            cs_0_1.PartType.Label = "";
            cs_0_1.PartType.Text  = "riga";
            cs_0_1.Description1.Label = "";
            cs_0_1.Description1.Text  = "avanti destra";
            cs_0_1.Description2.Label = "scuro";
            cs_0_1.Description2.Text  = "#CD031D";
            cs_0_1.Description3.Label = "";
            cs_0_1.Description3.Text  = "spalle calate";
            Characteristic cs_0_2 = new Characteristic();
            cs_0_2.Name.Label = "";
            cs_0_2.Name.Text  = "";
            cs_0_2.PartType.Label = "";
            cs_0_2.PartType.Text  = "";
            cs_0_2.Description1.Label = "";
            cs_0_2.Description1.Text  = "tasca destra";
            cs_0_2.Description2.Label = "";
            cs_0_2.Description2.Text  = "-";
            cs_0_2.Description3.Label = "";
            cs_0_2.Description3.Text  = "con risvolto";
            cs_0.Details.Add(cs_0_0);
            cs_0.Details.Add(cs_0_1);
            cs_0.Details.Add(cs_0_2);
            clothCard.Clothes = cs_0;
            result.OutfitTab = clothCard;

            return result;
        }

        private static BitmapImage getImage()
        {
            string imageFile = Path.GetFullPath(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"..\..\", @"image\", "event-icon.png"));

            BitmapImage bmp = new BitmapImage();

            bmp.BeginInit();
            bmp.UriSource = new Uri(imageFile);
            bmp.EndInit();

            return bmp;
        }
    }
}
