﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
//------------------------------
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TestJson.Json.Converters
{
    /// <summary>
    /// Da: https://stackoverflow.com/questions/44370046/how-do-i-serialize-object-to-json-using-json-net-which-contains-an-image-propert
    /// </summary>
    public class JsonImageConverter : JsonConverter
    {
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            BitmapImage image = new BitmapImage();
            var base64 = (string)reader.Value;

            // convert base64 to byte array, put that into memory stream and feed to image
            using (MemoryStream byteStream = new MemoryStream(Convert.FromBase64String(base64)))
            {
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.StreamSource = byteStream;
                image.EndInit();
            }

            return image;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            BitmapImage image = (BitmapImage)value;

            BitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));

            using (MemoryStream byteStream = new MemoryStream())
            {
                encoder.Save(byteStream);

                byte[] imageBytes = byteStream.ToArray();
                writer.WriteValue(imageBytes);
            }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(BitmapImage);
        }
    }
}
